# Condition: only run this action for the "content-length" header.
$1 == "Content-Length:" {
	# Store the Content Length to know when the entire request body
	# has been received. The content length header has the format of
	# "Content-Length: n". So the value is separated from the key by
	# a space and in the second field. By adding `+ 0`, it's cast to
	# a number.
	contentLength = $2 + 0;
}
# When the program comes across an empty line for the first time,
# the headers have been sent and now the body can be parsed.
/^\s*$/ {
	parsingBody = 1;

	# As the body is a CSV file and is separated by semicolons, we
	# now change the field separator to semicolons (rather than
	# spaces before, which was practical to parse the header)
	FS=";";
}

# The actions in this block are only performed when the headers have been
# parsed and now it's the body's turn.
parsingBody {
	# The second CSV column contains the values to sum up. 
	sum += $2;
	count ++;

	# To know when the entire request body has been sent, it is 
	# collected in the body variable.
	body=body $0 RS;
}


# Once the body variable is at least as long as the content 
# length, all data have been sent by the client and the
# program can exit.
parsingBody && length(body) >= contentLength {
	exit;
}



# When every line was parsed, return the sum.
END {
	print "HTTP/1.0 200 OK";
	print "";
	printf("Sum: %d, Count: %d, Avg: %d", sum, count, (sum / count));
	print ""
}
