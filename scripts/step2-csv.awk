BEGIN {
	# set the field separator to a semicolon
	FS=";";
}
{
	# sum up values
	sum += $2;

	# count number of values
	count ++;
}
END {
	printf("Sum: %d, Count: %d, Avg: %d", sum, count, (sum / count));
}
